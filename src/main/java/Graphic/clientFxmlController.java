package Graphic;

import Logic.Units;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.*;

/**
 * Created by msade on 4/21/2017.
 */
public class clientFxmlController {
    @FXML
    TextField ipTextField;
    @FXML
    Button joinBtn;

    public void initialize(){
        joinBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Units.serverIP = ipTextField.getText();
                try {
                    DatagramSocket client = new DatagramSocket();
                    client.send(new DatagramPacket(new byte[1],1, InetAddress.getByName(Units.serverIP),14725));
                    System.out.println("client");

                    Parent root = FXMLLoader.load(getClass().getResource("/Game.fxml"));
                    Stage stage = (Stage)joinBtn.getScene().getWindow();
                    stage.setScene(new Scene(root));
                } catch (SocketException e) {
                    e.printStackTrace();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
