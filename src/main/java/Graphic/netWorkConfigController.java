package Graphic;

import Logic.Units;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by msade on 4/21/2017.
 */
public class netWorkConfigController {
    @FXML
    Button hostBtn;
    @FXML
    Button joinBtn;

    public void initialize() {
        hostBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
//                Units.setNetWork(Units.netWorkMod.server);
                try {
                    Parent root = FXMLLoader.load(getClass().getResource("/serverFxml.fxml"));
                    Stage stage =(Stage)hostBtn.getScene().getWindow();
                    stage.setScene(new Scene(root));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        joinBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
//                    Units.setNetWork(Units.netWorkMod.client);
                    Parent root = FXMLLoader.load(getClass().getResource("/clientFxml.fxml"));
                    Stage stage =(Stage)joinBtn.getScene().getWindow();
                    stage.setScene(new Scene(root));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });



    }
}
