package Graphic;

import Logic.Units;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by msade on 4/11/2017.
 */
public class ModController {
    @FXML
    Button netWorkBtn;
    @FXML
    Button comBtn;

    public void initialize() {

        comBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            Stage stage;
            Parent root;
            @Override
            public void handle(MouseEvent event) {
                Units.setGamemod(Units.gameMod.com);
                try {
                    root = FXMLLoader.load(getClass().getResource("/Game.fxml"));
                    stage = (Stage)comBtn.getScene().getWindow();
                    stage.setScene(new Scene(root));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        netWorkBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            Stage stage;
            Parent root;
            @Override
            public void handle(MouseEvent event) {
                try {
                    root = FXMLLoader.load(getClass().getResource("/netWorkConfig.fxml"));
                    stage = (Stage)comBtn.getScene().getWindow();
                    stage.setScene(new Scene(root));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
