package Graphic;

import Logic.*;
import Network.Client;
import Network.Server;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

import java.net.DatagramPacket;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.ResourceBundle;
import java.util.Scanner;

/**
 * Created by msade on 4/9/2017.
 */
public class GameController implements Initializable {
    Pan pan1 = new Pan();
    Pan pan2 = new Pan();
    Ball disc = new Ball();
    @FXML
    Circle playerPan;
    @FXML
    Circle comPan;
    @FXML
    Circle ball;
    @FXML
    Pane playGnd;
    @FXML
    Text comScore;
    @FXML
    Text playerScore;
    int score1, score2;


    public void goal() {
        disc.setPosition(Units.WIDTH() / 2, Units.HEIGTH() / 2);
        disc.setSpeed(0, 0);
    }


    public void initialize(URL location, ResourceBundle resources) {
        final double width, height, goalWidth, boarderThikness;
        width = 400;
        height = 700;
        goalWidth = 400 / 4;
        boarderThikness = 10;
        final double panRadius = 30;
        Units.config(width, height, goalWidth, boarderThikness);

        pan1.setShape(playerPan);
        disc.setShape(ball);
        pan2.setShape(comPan);
        pan1.getShape().setRadius(panRadius);
        pan2.getShape().setRadius(panRadius);
        disc.getShape().setRadius(panRadius / 2);
        pan2.setPosition(Units.WIDTH() / 2, Units.HEIGTH() / 2 - Units.HEIGTH() / 4);
        disc.setPosition(Units.WIDTH() / 2, Units.HEIGTH() / 2);
        playGnd.setPrefHeight(height);
        playGnd.setPrefWidth(width);
        playGnd.setOnMouseMoved(new EventHandler<MouseEvent>() {
            double prevX = 0, prevY = 0;
            double T = System.currentTimeMillis();

            @Override
            public void handle(MouseEvent event) {
                pan1.setPosition(event.getX(), event.getY());
                pan1.calculateSpeed(event.getX() - prevX, event.getY() - prevY, System.currentTimeMillis() - T);
                prevX = event.getX();
                prevY = event.getY();
                T = System.currentTimeMillis();
                playerScore.setText("" + score1);

            }
        });

        Thread check = new Thread(new Runnable() {
            double T = System.currentTimeMillis();

            @Override
            public void run() {
                while (true) {
                    disc.checkHits(pan1);
                    disc.checkHits(pan2);
                    disc.move(System.currentTimeMillis() - T);
                    T = System.currentTimeMillis();
                    playerScore.setText("" + score1);
                    if(Units.getGamemod() == Units.gameMod.com) {
                        pan2.setSpeed(disc.getSpeed('x')/2,disc.getSpeed('y') /2);
                        pan2.move(System.currentTimeMillis() - T);
                    } else if(Units.getNetWork() == Units.netWorkMod.server){
                        try {
                            Server server = new Server();
                            server.setSelfLocation(msgBuilder(pan1));
                            getParametrs(server.getClientLocation());
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    } else {
                        try {
                            Client client = new Client(Units.serverIP);
                            client.setSelfLocation(msgBuilder(pan1));
                            getParametrs(client.getClientLocation());
                        } catch (UnknownHostException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }
        });
        Thread goal = new Thread(new Runnable() {
            double T = System.currentTimeMillis();

            @Override
            public void run() {
                while (true) {
                    if (disc.getPosition('x') >= width / 2 - goalWidth && disc.getPosition('x') <= width / 2 + goalWidth && disc.getPosition('y') >= height - 4 * boarderThikness) {
                        if (System.currentTimeMillis() - T > 1000) {
                            score1++;
                            goal();
                            T = System.currentTimeMillis();

                        }
                        playerScore.setText("" + score1);
                    }
                }

            }
        });
        goal.start();
        check.start();
    }

    public String msgBuilder(Pan pan){
        StringBuilder s = new StringBuilder();
        DecimalFormat round = new DecimalFormat("##.#####");
        s.append(round.format(pan.getPosition('x')));
        s.append(round.format(pan.getPosition('y')));
        s.append(round.format(pan.getSpeed('x')));
        s.append(round.format(pan.getSpeed('y')));
        return s.toString();
    }

    public void getParametrs(String msg){
        pan2.setPosition(Units.WIDTH() - Double.parseDouble(msg.substring(0,8)) ,
                Units.HEIGTH() - Double.parseDouble(msg.substring(8,16)));
        pan2.setSpeed(-1 * Double.parseDouble(msg.substring(16,24)) ,
                -1 * Double.parseDouble(msg.substring(24,32)));
    }




}


