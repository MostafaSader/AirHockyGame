package Graphic;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.*;

/**
 * Created by msade on 4/21/2017.
 */
public class serverFxmlController {
    @FXML
    Label ipLabel;
    @FXML
    Button btn;

    public void initialize() throws IOException {
        ipLabel.setText(InetAddress.getLocalHost().getHostAddress());
        btn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                DatagramPacket test = new DatagramPacket(new byte[1], 1);
                try {
                    new DatagramSocket(14725).receive(test);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    System.out.println("conect");
                    Parent root = FXMLLoader.load(getClass().getResource("/Game.fxml"));
                    Stage stage = (Stage) btn.getScene().getWindow();
                    stage.setScene(new Scene(root));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
