package Network;

/**
 * Created by msade on 4/21/2017.
 */
public class Server extends Thread{
    private UDPServer server;
    private String clientLocation;
    private String selfLocation;
    private boolean gameRun = false;
    @Override
    public void run(){
        try {
            server = new UDPServer(14725);
            do {
                clientLocation = server.receiveMessage();
                server.sendMessage(selfLocation);
            }while (gameRun);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startServer(){
        gameRun = true;
        start();
    }

    public void StopServer(){
        gameRun = false;
    }

    public String getClientLocation() {
        return clientLocation;
    }

    public String getSelfLocation() {
        return selfLocation;
    }

    public void setClientLocation(String clientLocation) {
        this.clientLocation = clientLocation;
    }

    public void setSelfLocation(String selfLocation) {
        this.selfLocation = selfLocation;
    }
}
