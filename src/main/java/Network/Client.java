package Network;

import java.net.UnknownHostException;

/**
 * Created by msade on 4/21/2017.
 */
public class Client extends Thread{
    private UDPClient client;
    private String clientLocation;
    private String selfLocation;
    private boolean gameRun = false;
    private String serverAddress;
    public Client(String ip) throws UnknownHostException {
        serverAddress = ip;

    }
    @Override
    public void run(){
        try {
            client = new UDPClient(serverAddress,14725);
            do {
                clientLocation = client.recievedMessage();
                client.sendMessage(selfLocation);
            }while (gameRun);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startServer(){
        gameRun = true;
        start();
    }

    public void StopServer(){
        gameRun = false;
    }

    public String getClientLocation() {
        return clientLocation;
    }

    public String getSelfLocation() {
        return selfLocation;
    }

    public void setClientLocation(String clientLocation) {
        this.clientLocation = clientLocation;
    }

    public void setSelfLocation(String selfLocation) {
        this.selfLocation = selfLocation;
    }
}
