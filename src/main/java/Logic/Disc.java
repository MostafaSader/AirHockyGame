package Logic;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * Created by msade on 4/9/2017.
 */
public abstract class Disc {
    private double speedX,speedY,x,y;
    private Circle shape = new Circle();
    Color color;

    public void calculateSpeed(double deltaX,double deltaY, double deltaT){
        setSpeed(deltaX / deltaT,deltaY / deltaT);
    }
    public double getSpeed(char xy){
        if(xy == 'x')
            return speedX;
        else return speedY;
    }

    public void setSpeed(double speedX,double speedY){
        this.speedX = speedX;
        this.speedY = speedY;
         if(speedX > 1.68)
             this.speedX = 1.68;
         if ((speedY > 1.68))
             this.speedY = 1.68;


    }
    public void setPosition(double x,double y){
        this.x = x;
        this.y = y;
        shape.setTranslateX(this.x);
        shape.setTranslateY(this.y);
    }
    public void setShape(Circle shape){
        this.shape = shape;

    }

    public void move(double deltaT){
        setPosition( (getSpeed('x') * deltaT  + x)
                , (getSpeed('y') * deltaT + y));

    }
    public double getPosition(char xy){
        if(xy == 'x')
            return this.x;
        else return this.y;
    }
    public Circle getShape(){
        return shape;
    }

}
