package Logic;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        final double width, height, goalWidth, boarderThikness;
        width = 400;
        height = 700;
        goalWidth = 400 / 4;
        boarderThikness = 10;
        double panRadius = 30;
        Units.config(width, height, goalWidth, boarderThikness);
        Parent root = FXMLLoader.load(getClass().getResource("/Mod.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.setWidth(Units.WIDTH() + 2 * Units.BOARDERTHIKNESS());
        primaryStage.setHeight(Units.HEIGTH() + 4 * Units.BOARDERTHIKNESS() + 5);
        primaryStage.show();
        System.out.println(primaryStage.getHeight()+ " " + root.getLayoutY());
    }
    public static void main(String[] args) {
        launch(args);
    }
}

