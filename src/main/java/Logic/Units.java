package Logic;

/**
 * Created by msade on 4/9/2017.
 */
public class Units {
    public static gameMod getGamemod() {
        return gamemod;
    }

    public static void setGamemod(gameMod gamemod) {
        Units.gamemod = gamemod;
    }

    public enum netWorkMod{
        client,server;
    };

    public enum gameMod{
        com,LAN
    };
    private static gameMod gamemod;
    private static  double width,height,goalWidth,boarderThikness,zero = 0;
    private static netWorkMod netWork;
    public static String serverIP;


    public static void config(double widths,double heights,double goalWidths, double boarderThiknesss){
        width = widths;
        height = heights;
        goalWidth = goalWidths;
        boarderThikness = boarderThiknesss;
    }

    public static double WIDTH() {
        return width;
    }

    public static double HEIGTH() {
        return height;
    }

    public static double GOALWIDTH() {
        return goalWidth;
    }

    public static double BOARDERTHIKNESS() {
        return boarderThikness;
    }
    public static double ZERO(){
        return zero;
    }

    public static void setNetWork(netWorkMod mod){
        netWork = mod;
    }
    public static netWorkMod getNetWork(){
        return netWork;
    }



}
