package Logic;

/**
 * Created by msade on 4/9/2017.
 */
public class Ball extends Disc {
    private void  overFieldCheck(){
        double ConstSpeedX = getSpeed('x');
        double ConstSpeedY = getSpeed('y');

        boolean hitRight = getPosition('x')  >= Units.WIDTH() - Units.BOARDERTHIKNESS();
        boolean hitLeft  = getPosition('x')  <= Units.ZERO()  + Units.BOARDERTHIKNESS();
        boolean hitDown  = getPosition('y')  >= Units.HEIGTH()- Units.BOARDERTHIKNESS();
        boolean hitUP    = getPosition('y')  <= Units.ZERO()  + Units.BOARDERTHIKNESS();

        if(hitRight || hitLeft) {
            if (hitRight)
                setPosition(Units.WIDTH() - getShape().getRadius(), getPosition('y'));
            if(hitLeft)
                setPosition(Units.ZERO() + getShape().getRadius(),getPosition('y'));
            setSpeed(-1 * getSpeed('x'), ConstSpeedY);

        }
        if(hitDown  || hitUP) {
            if (hitUP)
                setPosition(getPosition('x'), Units.ZERO() + getShape().getRadius());
            if (hitDown)
                setPosition(getPosition('x'), Units.HEIGTH() - getShape().getRadius());
            setSpeed(ConstSpeedX, -1 * getSpeed('y'));

        }
    }

    private void checkPanHit(Pan pan) {
        double realDistance = Math.sqrt(Math.pow(getPosition('x') - pan.getPosition('x'), 2) +
                Math.pow(getPosition('y') - pan.getPosition('y'), 2));
        double hitDistance = getShape().getRadius() + pan.getShape().getRadius();
        boolean hit = realDistance <= hitDistance;
        if(hit)
            collision(pan);
    }




    private void collision(Pan pan){


        double speedX = pan.getSpeed('x') - getSpeed('x');
        double speedY = pan.getSpeed('y') - getSpeed('y');
        double rotationAngle = Math.atan(  (getPosition('y') - pan.getPosition('y') ) /
                                           (getPosition('x') - pan.getPosition('x') ) );
        double angle = -1 * Math.atan(getSpeed('y') / getSpeed('x'));
        double finalSpeedX = -1 * speedX * Math.cos(2 * rotationAngle) - speedY * Math.sin(2* rotationAngle) + pan.getSpeed('x');
        double finalSpeedY = speedY * Math.cos(2 * rotationAngle) - speedX * Math.sin(2 * rotationAngle) + pan.getSpeed('y');
        setSpeed(finalSpeedX,finalSpeedY);
        boolean isRight = getPosition('x') >= pan.getPosition('x');
        boolean isDown  = getPosition('y') >= pan.getPosition('y');
        double positionX = pan.getPosition('x');
        double positionY = pan.getPosition('y');
        double sumOfRadius = pan.getShape().getRadius() + getShape().getRadius();
        if(isRight) {
            positionY += Math.sin(rotationAngle) * sumOfRadius;
            positionX += Math.cos(rotationAngle) * sumOfRadius;
        }else {
            positionY += Math.sin(rotationAngle) * sumOfRadius * -1;
            positionX += Math.cos(rotationAngle) * sumOfRadius * -1;
        }
        if(pan.getPosition('x') == getPosition('x')){
            if(!isDown)
                setPosition(pan.getPosition('x'),sumOfRadius);
            else setPosition(pan.getPosition('x'),-1 * sumOfRadius);
            setSpeed(getSpeed('x'),-1 * getSpeed('y'));
            return;
        }
        if(pan.getPosition('y') == getPosition('y')){
            if(isRight)
                setPosition(sumOfRadius,getPosition('y'));
            else setPosition(-1 * sumOfRadius,getPosition('y'));
            setPosition(-1 * getSpeed('x'),getPosition('y'));
            return;
        }
        setSpeed(finalSpeedX,finalSpeedY);
        setPosition(positionX,positionY);

    }

    public void  checkHits(Pan pan){
        checkPanHit(pan);
        overFieldCheck();
    }


    public byte playerHasGoal(){
        if(getPosition('x') < Units.WIDTH() /2 + Units.BOARDERTHIKNESS() /2 &&
                getPosition('x') > Units.WIDTH() /2 - Units.BOARDERTHIKNESS() /2 &&
                getPosition('y') < Units.HEIGTH() - Units.BOARDERTHIKNESS())
            return 1;
        if(getPosition('x') < Units.WIDTH() /2 + Units.BOARDERTHIKNESS() /2 &&
                getPosition('x') > Units.WIDTH() /2 - Units.BOARDERTHIKNESS() /2 &&
                getPosition('y') < Units.ZERO() + Units.BOARDERTHIKNESS())
            return 2;
        return 0;
    }
}
